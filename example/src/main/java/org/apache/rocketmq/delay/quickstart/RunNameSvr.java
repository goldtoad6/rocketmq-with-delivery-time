package org.apache.rocketmq.delay.quickstart;

import org.apache.rocketmq.namesrv.NamesrvStartup;

import static org.apache.rocketmq.delay.quickstart.ProjectRoot.rootPath;

public class RunNameSvr {

    public static void main(String[] args) {
        System.setProperty("rocketmq.home.dir", rootPath);
        System.setProperty("user.home", rootPath);
        NamesrvStartup.main(args);

    }
}
