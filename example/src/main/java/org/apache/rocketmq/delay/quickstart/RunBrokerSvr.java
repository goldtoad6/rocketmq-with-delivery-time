package org.apache.rocketmq.delay.quickstart;

import org.apache.rocketmq.broker.BrokerStartup;

import static org.apache.rocketmq.delay.quickstart.ProjectRoot.rootPath;

public class RunBrokerSvr {

    public static void main(String[] args) {

        //需要添加 -n localhost:9876 启动参数
        //工程代码编译的路径
        System.setProperty("rocketmq.home.dir", rootPath);
        System.setProperty("user.home", rootPath);
        BrokerStartup.main(args);

    }
}
