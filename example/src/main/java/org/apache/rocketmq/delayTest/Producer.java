/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.rocketmq.delayTest;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;


public class Producer {
    public static void main(String[] args) throws MQClientException, InterruptedException {


        DefaultMQProducer producer = new DefaultMQProducer("please_rename_unique_group_name");

        producer.setNamesrvAddr("localhost:9876");


        producer.start();
        for (int i = 0; i < 5; i++) {
            try {
                long randNum = 5000; //RandomUtils.nextLong(1000,5000);
                long deliverTime = System.currentTimeMillis() + randNum;
                String deliverTimeStr = DateFormatUtils.format(deliverTime,"yyyy-MM-dd HH:mm:ss");

                long currDate = System.currentTimeMillis();
                String currDateStr = DateFormatUtils.format(currDate,"yyyy-MM-dd HH:mm:ss");
                String body="test测试";
                  Message msg = new Message("TopicTest3" /* Topic */,
                        "TagA" /* Tag */,
                        ( i + ",delayTime:" + randNum + "ms,send time:" + currDateStr + ",deliverTime:" + deliverTimeStr + ",body:" + body ).getBytes(RemotingHelper.DEFAULT_CHARSET) /* Message body */
                );
                msg.setStartDeliverTime(deliverTime);
                //msg.putUserProperty(Message.SystemPropKey.STARTDELIVERTIME, String.valueOf(deliverTime));
                //msg.setDelayTimeLevel(2);
                SendResult sendResult = producer.send(msg);

                System.out.printf("%d, %s %n", i, sendResult);
                Thread.sleep(500);
            } catch (Exception e) {
                e.printStackTrace();

            }
        }


        /*
         * Shut down once the producer instance is not longer in use.
         */
        producer.shutdown();
    }
}
