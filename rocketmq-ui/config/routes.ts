﻿export default [
  {
    path: '/topic',
    name: 'Topic 管理',
    icon: 'FontSizeOutlined',
    component: './topic/Topic',
  },
  {
    path: '/group',
    name: 'Group 管理',
    icon: 'GoogleOutlined',
    component: './group/Group',
  },
  {
    path: '/message',
    name: '消息查询',
    icon: 'AliwangwangOutlined',
    component: './message/Message',
  },
  {
    path: '/cluster',
    name: 'Broker管理',
    icon: 'ClusterOutlined',
    component: './cluster/Cluster',
  },
  {
    path: '/dashboard',
    name: '消息分析',
    icon: 'DashboardOutlined',
    component: './dashboard/DashBoard',
  },
  /**
  {
    path: '/trace',
    name: '消息轨迹',
    icon: 'PieChartOutlined',
    component: './Trace',
  },
  {
    path: '/dieQueue',
    name: '死信队列',
    icon: 'SendOutlined',
    component: './DieQueue',
  },
*/

  {
    path: '/',
    redirect: '/topic',
  },
  {
    component: './404',
  },
];
