import { message } from 'antd'

export function FormatDateTime(timeStamp: number) {
  const date = new Date()

  date.setTime(timeStamp * 1000)

  const y = date.getFullYear()

  let m: string | number = date.getMonth() + 1

  m = m < 10 ? '0' + m : m

  let d: string | number = date.getDate()

  d = d < 10 ? '0' + d : d

  let h: string | number = date.getHours()

  h = h < 10 ? '0' + h : h

  let minute: string | number = date.getMinutes()

  let second: string | number = date.getSeconds()

  minute = minute < 10 ? '0' + minute : minute

  second = second < 10 ? '0' + second : second

  return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second
}

export function FormatTime(timeStamp: number) {
  const date = new Date()

  date.setTime(timeStamp * 1000)

  let h: string | number = date.getHours()

  h = h < 10 ? '0' + h : h

  let minute: string | number = date.getMinutes()

  let second: string | number = date.getSeconds()

  minute = minute < 10 ? '0' + minute : minute

  second = second < 10 ? '0' + second : second

  return h + ':' + minute + ':' + second
}

export function FormatDate(timeStamp: number) {
  const date = new Date()

  date.setTime(timeStamp * 1000)

  const y = date.getFullYear()

  let m: string | number = date.getMonth() + 1

  m = m < 10 ? '0' + m : m

  let d: string | number = date.getDate()

  d = d < 10 ? '0' + d : d

  return y + '-' + m + '-' + d
}

export function ToStringByte(limit: number) {
  let size = ''
  if (limit < 0.1 * 1024) {
    //小于0.1KB，则转化成B
    size = limit.toFixed(2) + 'B'
  } else if (limit < 0.1 * 1024 * 1024) {
    //小于0.1MB，则转化成KB
    size = (limit / 1024).toFixed(2) + 'KB'
  } else if (limit < 0.1 * 1024 * 1024 * 1024) {
    //小于0.1GB，则转化成MB
    size = (limit / (1024 * 1024)).toFixed(2) + 'MB'
  } else {
    //其他转化成GB
    size = (limit / (1024 * 1024 * 1024)).toFixed(2) + 'GB'
  }

  let sizeStr = size + '' //转成字符串
  let index = sizeStr.indexOf('.') //获取小数点处的索引
  let dou = sizeStr.substr(index + 1, 2) //获取小数点后两位的值
  if (dou == '00') {
    //判断后两位是否为00，如果是则删除00
    return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
  }
  return size
}

export default function onCopy(certB64: {} | null | undefined, title: string) {
  let oInput = document.createElement('input')
  if (typeof certB64 === "string") {
    oInput.value = certB64
  }
  document.body.appendChild(oInput)
  oInput.select() // 选择对象
  document.execCommand('Copy') // 执行浏览器复制命令
  message.success(title + '复制成功', 1)
  oInput.remove()
}
