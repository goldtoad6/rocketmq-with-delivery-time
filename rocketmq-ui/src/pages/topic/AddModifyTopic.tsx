import HttpRequest from '@/utils/HttpRequest';
import { defaultDomain } from '@/config/domain';
import { BetaSchemaForm } from '@ant-design/pro-form'
import { Button, Form, message } from 'antd'
import { useRef, useState } from 'react'

export interface UpdateTopic {
  brokerName: string
  readQueueNums: number
  brokerNameList: []
  clusterNameList: []
  order: boolean
  perm: number
  topicName: string
  writeQueueNums: number
}

export function AddModifyTopic(props: { record: { dv: boolean } }) {
  const formRef = useRef()
  const [form] = Form.useForm()
  const [_, setLoading] = useState(false)
  const [clusterList, setClusterList] = useState([])
  const [brokerNameList, setBrokerNameList] = useState([])
  return (
    <BetaSchemaForm<UpdateTopic>
      form={form}
      formRef={formRef}
      key="AddTopic"
      title="添加主题"
      trigger={<Button type="primary">新增/更新topic</Button>}
      layoutType="ModalForm"
      width={800}
      onFinish={async (values) => {
        console.log('>>update >> ', values)
        const res = await HttpRequest({
          baseURL: defaultDomain,
          url: '/topic/createOrUpdate.do',
          method: 'POST',
          params: values,
        })
        // @ts-ignore
        const status = res.status
        if (status === 0) {
          message.success('更新成功')
        } else {
          message.error('更新失败' + res.errMsg)
          return false
        }
        return true
      }}
      onVisibleChange={async (v) => {
        props.record.dv = v
        console.log('rv=', v)
        if (v) {
          setLoading(true)
          const res = await HttpRequest({
            baseURL: defaultDomain,
            url: '/cluster/list.query',
            method: 'GET',
            params: {},
          })
          // @ts-ignore
          const data = res.data
          console.log('>>>>>>>>>>', data)
          const broList = []
          const cluList = []
          if (data) {
            if (data.clusterInfo && data.clusterInfo.brokerAddrTable) {
              const broTabList = data.clusterInfo.brokerAddrTable
              for (const n in broTabList) {
                const d = broTabList[n]
                cluList.push({ label: d.cluster, value: d.cluster })
                broList.push({ label: d.brokerName, value: d.brokerName })
              }
            }
          }
          // @ts-ignore
          setBrokerNameList(broList)
          // @ts-ignore
          setClusterList(cluList)
          // @ts-ignore
          formRef.current.setFieldsValue({
            //clusterNameList: cluList,
            // brokerNameList: broList,
          })
          setLoading(false)
        }
      }}
      columns={[
        {
          title: '集群名',
          dataIndex: 'clusterNameList',
          valueType: 'select',
          formItemProps: {
            rules: [
              {
                required: true,
                message: '请输入集群名',
              },
            ],
          },
          fieldProps: {
            mode: 'multiple',
            placeholder: '集群名',
            options: clusterList,
          },
        },
        {
          title: 'BROKER_NAME',
          dataIndex: 'brokerNameList',
          valueType: 'select',
          formItemProps: {
            rules: [
              {
                required: true,
                message: '请输入订阅组',
              },
            ],
          },
          fieldProps: {
            mode: 'multiple',
            placeholder: '订阅组',
            options: brokerNameList,
          },
        },

        {
          title: '主题名',
          dataIndex: 'topicName',
          formItemProps: {
            rules: [
              {
                required: true,
                message: '请输入主题名',
              },
            ],
          },
        },
        {
          title: '读队列数量',
          dataIndex: 'readQueueNums',
          initialValue: 16,
          formItemProps: {
            rules: [
              {
                required: true,
                message: '请输入读队列数量',
              },
            ],
          },
        },
        {
          title: '写队列数量',
          dataIndex: 'writeQueueNums',
          initialValue: 16,
          formItemProps: {
            rules: [
              {
                required: true,
                message: '请输入写队列数量',
              },
            ],
          },
        },
        {
          title: 'perm',
          dataIndex: 'perm',
          initialValue: 6,
          formItemProps: {
            rules: [
              {
                required: true,
                message: '请输入perm',
              },
            ],
          },
        },
      ]}
    />
  )
}
