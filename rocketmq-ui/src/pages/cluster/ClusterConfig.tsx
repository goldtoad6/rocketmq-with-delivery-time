import {Button, Col, Row, Tabs} from 'antd';
import ProTable from '@ant-design/pro-table';
import {ModalForm} from '@ant-design/pro-form';
import HttpRequest from '@/utils/HttpRequest';
import {defaultDomain} from '@/config/domain';
import {useState} from "react";


const { TabPane } = Tabs

export function ClusterConfig(prop: {
  record: { cv: boolean; adder: string; status: {} }
}) {
  const [statusList, setStatusList] = useState([])
  const [confList, setConfList] = useState([])
  return (
    <ModalForm
      key="clusterView"
      title={`[${prop.record.adder}]`}
      onVisibleChange={async (v) => {
        prop.record.cv = v
        if (v) {
          const res = await HttpRequest({
            baseURL: defaultDomain,
            url: '/cluster/brokerConfig.query?brokerAddr=' + prop.record.adder,
            method: 'GET',
            params: [],
          })
          // @ts-ignore
          const data = res.data
          console.log('res=', data)
          let dataList = []
          if (data) {
            for (let d in data) {
              let item = data[d]
              dataList.push({ key: d, value: item })
            }
          }
          // @ts-ignore
          setConfList(dataList)

          let recordData = prop.record.status
          console.log('recordData>>', recordData)

          let statusList = []
          if (recordData) {
            for (let d in recordData) {
              let item = recordData[d]
              statusList.push({ key: d, value: item })
            }
          }
          // @ts-ignore
          setStatusList(statusList)
        }
      }}
      trigger={<Button type="link">详情</Button>}
      width="80%"
      onFinish={async () => true}
    >
      <div
        className="site-card-wrapper"
        style={{ height: '500px', overflowY: 'auto' }}
      >
        <Tabs defaultActiveKey="1">
          <TabPane tab="状态" key="1">
            <Row gutter={24}>
              <Col span={20}>
                <ProTable
                  rowKey="key"
                  search={false}
                  toolbar={[]}
                  toolBarRender={false}
                  scroll={{ x: 800 }}
                  pagination={{ pageSize: 200 }}
                  dataSource={statusList}
                  columns={[
                    { title: 'key', dataIndex: 'key', search: false },
                    { title: 'value', dataIndex: 'value', search: false },
                  ]}
                />
              </Col>
            </Row>
          </TabPane>

          <TabPane tab="配置" key="2">
            <Row gutter={24}>
              <Col span={20}>
                <ProTable
                  rowKey="key"
                  search={false}
                  toolbar={[]}
                  toolBarRender={false}
                  scroll={{ x: 800 }}
                  pagination={{ pageSize: 200 }}
                  dataSource={confList}
                  columns={[
                    {
                      title: 'key',
                      dataIndex: 'key',
                      search: false,
                    },
                    {
                      title: 'value',
                      dataIndex: 'value',
                      search: false,
                    },
                  ]}
                />
              </Col>
            </Row>
          </TabPane>
        </Tabs>
      </div>
    </ModalForm>
  )
}
