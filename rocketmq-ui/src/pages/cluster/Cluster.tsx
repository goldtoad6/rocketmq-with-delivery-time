import ProTable from '@ant-design/pro-table';
import HttpRequest from '@/utils/HttpRequest';
import {defaultDomain} from '@/config/domain';
import React, {useState} from "react";
import {ClusterConfig} from "@/pages/cluster/ClusterConfig";
import {PageContainer} from "@ant-design/pro-layout";


const Cluster: React.FC = (prop) => {
  const [dataSource, setDataSource] = useState([])
  const [loading, setLoading] = useState(false)

  const queryParams = [
    { title: '分片', align: 'center', dataIndex: 'broker', search: false },
    { title: '编号', align: 'center', dataIndex: 'index', search: false },
    { title: '地址', align: 'center', dataIndex: 'adder', search: false },
    {
      title: '版本',
      align: 'center',
      dataIndex: 'brokerVersionDesc',
      search: false,
    },
    {
      title: '生产消息TPS',
      align: 'center',
      dataIndex: 'putTps',
      search: false,
    },
    {
      title: '消费消息TPS',
      align: 'center',
      dataIndex: 'getTps',
      search: false,
    },
    {
      title: '昨日生产总数',
      align: 'center',
      dataIndex: 'yesterdayPut',
      search: false,
    },
    {
      title: '昨日消费总数',
      align: 'center',
      dataIndex: 'yesterdayGet',
      search: false,
    },
    {
      title: '今天生产总数',
      align: 'center',
      dataIndex: 'todayPut',
      search: false,
    },
    {
      title: '今天消费总数',
      align: 'center',
      dataIndex: 'todayGet',
      search: false,
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'action',
      search: false,
      render: (dom: any, record: any) => {
        return [<ClusterConfig key={'id'} record={record} />]
      },
    },
  ]
  return <PageContainer key="cluster">
    <ProTable
  rowKey={'id'}
  search={{labelWidth: 'auto'}}
  scroll={{x: 800}}
  pagination={{pageSize: 20}}
  toolBarRender={false}
  dataSource={dataSource}
  loading={loading}
  // @ts-ignore
  request={async (params) => {
    setLoading(true)
    const res = await HttpRequest({
      baseURL: defaultDomain,
      url: '/cluster/list.query',
      method: 'GET',
      params: params,
    })
    console.log('res=', res)
    // @ts-ignore
    const data = res.data
    let brokerList = []
    if(data) {
      let brokerServer = data.brokerServer
      let clusterInfo = data.clusterInfo
      if (brokerServer) {
        for (let i in brokerServer) {
          let serverList = brokerServer[i]
          for (let s in serverList) {
            let instance = serverList[s]
            let bro = {}
            bro['broker'] = i
            if (clusterInfo && clusterInfo.brokerAddrTable) {
              let address = clusterInfo.brokerAddrTable[i]
              if (address && address.brokerAddrs) {
                bro['adder'] = address.brokerAddrs[0]
              }
            }
            // @ts-ignore
            bro['index'] = s == 0 ? s + ' (master)' : s + ' (slave)'
            bro['id'] = Math.random().toString()
            bro['brokerVersionDesc'] = instance.brokerVersionDesc
            bro['putTps'] = Number.parseFloat(
              instance.putTps.split(' ')[0]
            ).toFixed(2)
            bro['getTps'] = Number.parseFloat(
              instance.getTransferedTps.split(' ')[0]
            ).toFixed(2)

            bro['yesterdayPut'] =
              instance.msgPutTotalTodayMorning -
              instance.msgPutTotalYesterdayMorning
            bro['yesterdayGet'] =
              instance.msgGetTotalTodayMorning -
              instance.msgGetTotalYesterdayMorning

            bro['todayPut'] =
              instance.msgPutTotalTodayNow -
              instance.msgPutTotalTodayMorning
            // @ts-ignore
            bro['todayGet'] =
              instance.msgGetTotalTodayNow -
              instance.msgGetTotalTodayMorning
            bro['status'] = instance
            brokerList.push(bro)
          }
        }
      }
      // @ts-ignore
      setDataSource(brokerList)
    }
    setLoading(false)
  }}
  // @ts-ignore
  columns={queryParams}/>
  </PageContainer>
}

export default Cluster
