import HttpRequest from '@/utils/HttpRequest';
import {defaultDomain} from '@/config/domain';
import {ModalForm, ProFormInstance} from '@ant-design/pro-form'
import {Button, Form, Input, message, Select, Switch} from 'antd'
import {useRef, useState} from 'react'

export function AddGroupView(props: any) {
  const [brokerNameList, setBrokerNameList] = useState([])
  const [clusterList, setClusterList] = useState([])

  const ref = useRef<ProFormInstance>()

  async function getClusterList() {
    const res = await HttpRequest({
      baseURL: defaultDomain,
      url: '/cluster/list.query',
      method: 'GET',
      params: [],
    })
    console.log('res=', res)
    // @ts-ignore
    const data = res.data
    let brokerServer = data.brokerServer
    let clusterInfo = data.clusterInfo
    if (brokerServer) {
      let broList = []
      for (let k in brokerServer) {
        broList.push(k)
      }
      // @ts-ignore
      setBrokerNameList(broList)
    }
    if (clusterInfo) {
      let cluster = clusterInfo.clusterAddrTable
      let cluList = []
      for (let k in cluster) {
        cluList.push(k)
      }
      // @ts-ignore
      setClusterList(cluList)
    }
  }

  async function doUpdateConsumer(v: any) {
    // console.log("search=", v)
    let p = {
      subscriptionGroupConfig: {
        groupName: v.groupName,
        consumeEnable: v.consumeEnable,
        consumeFromMinEnable: true,
        consumeBroadcastEnable: v.consumeBroadcastEnable,
        retryQueueNums: v.retryQueueNums,
        retryMaxTimes: 16,
        brokerId: v.brokerId,
        whichBrokerWhenConsumeSlowly: v.whichBrokerWhenConsumeSlowly,
      },
      brokerNameList: v.brokerName,
      clusterNameList: v.clusterName,
    }

    const res = await HttpRequest({
      baseURL: defaultDomain,
      url: '/consumer/createOrUpdate.do',
      method: 'POST',
      params: p,
    })
    // console.log("res=", res);
    const status = res.status
    if (status === 0) {
      message.info('更新成功')
      return true
    } else {
      message.error('更新失败:' + res.errMsg)
      return false
    }
  }

  return (
    <ModalForm
      layout="horizontal"
      labelAlign="right"
      labelCol={{ span: 12 }}
      formRef={ref}
      // @ts-ignore
      formKey={'AddUpdateConsumerView'}
      key="AddUpdateConsumerView"
      title={`添加订阅`}
      onVisibleChange={(v) => {
        if (v) {
          getClusterList()
        }
      }}
      trigger={<Button type="primary">新增Group</Button>}
      width="600px"
      onFinish={async (v) => {
        await doUpdateConsumer(v)
        return true
      }}
    >
      <Form.Item
        label="clusterName"
        name="clusterName"
        rules={[{ required: true }]}
      >
        <Select mode="multiple">
          {clusterList.map((item) => (
            <Select.Option value={item} key={item}>
              {item}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        label="brokerName"
        name="brokerName"
        rules={[{ required: true }]}
      >
        <Select mode="multiple">
          {brokerNameList.map((item) => (
            <Select.Option value={item} key={item}>
              {item}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        label={'groupName'}
        name="groupName"
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label={'consumeEnable'}
        name="consumeEnable"
        initialValue={true}
        valuePropName="checked"
      >
        <Switch />
      </Form.Item>
      <Form.Item
        label={'consumeBroadcastEnable'}
        valuePropName="checked"
        initialValue={true}
        name="consumeBroadcastEnable"
      >
        <Switch />
      </Form.Item>

      <Form.Item
        label={'retryQueueNums'}
        name="retryQueueNums"
        initialValue={1}
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label={'brokerId'}
        name="brokerId"
        initialValue={0}
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label={'whichBrokerWhenConsumeSlowly'}
        name="whichBrokerWhenConsumeSlowly"
        initialValue={1}
        rules={[{ required: true }]}
      >
        <Input />
      </Form.Item>
    </ModalForm>
  )
}

export default AddGroupView
