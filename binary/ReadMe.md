## 1.解压 rocketmq-4.9.2.zip文件，到你的目录 （如 /opt/data/rocketmq-4.9.2)

## 2.配置java环境 ，配置好 JAVA_HOME指向java 路径
export  JAVA_HOME=/你的java路径

## 3.进入到解压目录
cd /opt/data/rocketmq-4.9.2

## 4.启动 namesvr 服务
./bin/mqnamesrv

## 5.启动 broker
./bin/mqbroker  -c ./conf/broker.conf

## 6.用rocketmq-ui查看
java -jar rocketmq-console-1.0.1.jar

打开浏览器查看 http://localhost:8082/

## 7.测试 (example)
在项目 example\src\main\java\org\apache\rocketmq\example\quickstart
有发送和接受消息范例

